/* global cy */
/// <reference types="Cypress" />

describe('home page', () => {
  beforeEach(() => {
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/trending/movie/day?api_key=*&language=es-ES&include_adult=false',
      { fixture: './../../fixtures/movies_trending.json' }
    ).as('getTrendingMovies');
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/movie/top_rated?api_key=*&language=es-ES&include_adult=false',
      { fixture: './../../fixtures/movies_toprated.json' }
    ).as('getTopRatedMovies');
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/trending/tv/day?api_key=*&language=es-ES&include_adult=false',
      { fixture: './../../fixtures/tvshow_trending.json' }
    ).as('getTrendingTV');
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/tv/top_rated?api_key=*&language=es-ES&include_adult=false',
      { fixture: './../../fixtures/tvshow_toprated.json' }
    ).as('getTopRatedTV');
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/genre/movie/list?api_key=*&language=es-ES',
      { fixture: './../../fixtures/movies_genres.json' }
    ).as('getMoviesGenres');
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/genre/tv/list?api_key=*&language=es-ES',
      { fixture: './../../fixtures/tv_genres.json' }
    ).as('getTVGenres');
    cy.visit('/');
    cy.wait(['@getTrendingMovies', '@getTopRatedMovies', '@getTrendingTV', '@getTopRatedTV', '@getMoviesGenres', '@getTVGenres'])
  });
  it('displays home page with 4 sections', () => {
    cy
      .get('main')
      .as('main');
    cy
      .get('@main')
      .contains('Bienvenidos');
    cy
      .get('@main')
      .find('section')
      .should('have.length', 4);
    cy
      .get('@main')
      .find('section')
      .find('tmdb-trending');
    cy
      .get('@main')
      .find('section')
      .find('h2');
  });
  it('each section has a list of cards displayed in landscape mode', () => {
    cy
      .get('main')
      .as('main');
    cy
      .get('@main')
      .find('section')
      .first()
      .find('tmdb-cards-list-ui')
      .should('have.attr', 'landscape');
  });
});
