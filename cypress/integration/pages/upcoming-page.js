/* global cy */
/// <reference types="Cypress" />

describe("upcoming page", () => {
  beforeEach(() => {
    cy.intercept(
      "GET",
      `https://api.themoviedb.org/3/movie/upcoming?api_key=*&language=es-ES&region=ES&include_adult=false`,
      { fixture: "./../../fixtures/upcoming.json" }
    ).as('getUpcoming');
    cy.visit("/upcoming");
    cy.wait('@getUpcoming');
  });
  it("calendar is displayed with current month", () => {
    cy.get("custom-calendar").shadow().as("calendar");
    cy.get("@calendar").contains(new Date().getFullYear().toString());
    const month = new Intl.DateTimeFormat("es", { month: "long" }).format(
      new Date().setMonth(new Date().getMonth())
    );
    const capitalizedMonth =
      month.slice(0, 1).toUpperCase() + month.slice(1).toLowerCase();
    cy.get("@calendar").contains(capitalizedMonth);
  });

  it("can change to next month", () => {
    cy.viewport(1600, 1400);
    cy.get("custom-calendar").shadow().as("calendar");
    cy.get("@calendar").find("#next-month-control").contains(">").click();
    const month = new Intl.DateTimeFormat("es", { month: "long" }).format(
      new Date().setMonth(new Date().getMonth() + 1)
    );
    const capitalizedMonth =
      month.slice(0, 1).toUpperCase() + month.slice(1).toLowerCase();
    cy.get("@calendar").contains(capitalizedMonth);
  });

  it("can change to prev month", () => {
    cy.viewport(1600, 1400);
    cy.get("custom-calendar").shadow().as("calendar");
    cy.get("@calendar").find("#prev-month-control").contains("<").click();
    const month = new Intl.DateTimeFormat("es", { month: "long" }).format(
      new Date().setMonth(new Date().getMonth() - 1)
    );
    const capitalizedMonth =
      month.slice(0, 1).toUpperCase() + month.slice(1).toLowerCase();
    cy.get("@calendar").contains(capitalizedMonth);
  });
});
