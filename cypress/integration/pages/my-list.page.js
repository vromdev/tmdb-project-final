import SNAPSHOT from '../../../fixtures/snapshot.json';
/* global cy */
/// <reference types="Cypress" />

describe('my list page', () => {
  beforeEach(() => {
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/movie/508947?api_key=*&language=es-ES',
      { fixture: './../../fixtures/movie_detail.json' }
    ).as('getMovie');
    cy.intercept(
      'GET',
      'https://api.themoviedb.org/3/movie/508947/videos?api_key=*',
      { fixture: './../../fixtures/movie_videos.json' }
    ).as('getVideos');
    cy.visit('/movie/508947/red');
    cy.wait(['@getMovie', '@getVideos']);
  });
  it('add to list button is displayed and clickable', () => {
    cy.viewport(1400, 1200);
    cy
      .get('tmdb-detailed-view-ui')
      .shadow()
      .find('tmdb-add-list-button')
      .shadow()
      .findByRole('button')
      .as('addListButton');
    cy
      .get('@addListButton')
      .contains('Añadir a Mi lista');
    cy
      .get('@addListButton')
      .click();
    cy
      .get('@addListButton')
      .contains('Eliminar de Mi lista');
  });
});
describe('my list page (localstorage)', () => {
  beforeEach(() => {
    localStorage.setItem('genktv-my-list', JSON.stringify(SNAPSHOT));
    cy.visit('/my-list');
  });
  it('my list page displays the movies and tv shows stored in localstorage', () => {
    cy
      .get('tmdb-my-list')
      .children()
      .should('have.length', 2);
    cy
      .get('tmdb-cards-list-ui')
      .first()
      .shadow()
      .findByRole('list')
      .as('moviesList');
    cy
      .get('@moviesList')
      .children()
      .first()
      .contains('Red');
    cy
      .get('@moviesList')
      .children()
      .contains('El Padrino');
  });
});
