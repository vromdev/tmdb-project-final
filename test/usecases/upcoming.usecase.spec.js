import { TMDBRepository } from "../../src/repositories/tmdb.repository";
import UPCOMING from "../../fixtures/upcoming.json";

import { UpcomingUseCase } from "../../src/usecases/upcoming.usecase";

jest.mock("../../src/repositories/tmdb.repository");

describe("Upcoming use case...", () => {
  beforeEach(() => {
    TMDBRepository.mockClear();
  });

  it("should execute correctly, returning a new object with the upcoming movies", async () => {
    TMDBRepository.mockImplementation(() => {
      return {
        getUpcoming: () => UPCOMING,
      };
    });

    const useCase = new UpcomingUseCase();
    const response = await useCase.execute();
    expect(Object.keys(response)).toEqual(["results"]);

    const { results } = response;
    expect(Object.keys(results)).toEqual([
      "20/5/2022",
      "3/6/2022",
      "27/5/2022",
      "9/6/2022",
      "26/5/2022",
    ]);
    expect(results["20/5/2022"]).toHaveLength(8);
    expect(results["20/5/2022"]).toEqual([
      "El sastre de la mafia",
      "El arma del engaño",
      "Juego de asesinos",
      "Corazón de Fuego",
      "Espejo, espejo",
      "Cinco lobitos",
      "¡Dolores guapa!",
      "JFK: Caso revisado",
    ]);
  });
});
