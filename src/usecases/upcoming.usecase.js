import { TMDBRepository } from "../repositories/tmdb.repository";

export class UpcomingUseCase {
  async execute() {
    const repository = new TMDBRepository();
    const { results } = await repository.getUpcoming();
    /* eslint-disable camelcase */
    const resultsWithAllData = results
      .map(({ release_date, title }) => {
        return { date: release_date, name: title };
      })
      .reduce((acc, curr) => {
        const date = new Date(curr.date).toLocaleDateString();
        if (acc.hasOwnProperty(date)) {
          acc[date].push(curr.name);
        } else {
          acc[date] = [curr.name];
        }
        return acc;
      }, {});

    return {
      results: resultsWithAllData,
    };
  }
}
