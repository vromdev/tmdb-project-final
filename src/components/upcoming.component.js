import { html, LitElement } from "lit";

import { UpcomingUseCase } from "../usecases/upcoming.usecase";
import "@vrom-dev/custom-calendar/custom-calendar";
export class UpcomingComponent extends LitElement {
  static get properties() {
    return {
      results: {
        type: Object,
        state: true,
      },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    const useCase = new UpcomingUseCase();
    const response = await useCase.execute();
    const { results } = response;
    this.results = results;
  }

  render() {
    return html`<custom-calendar
      locale="es-ES"
      .events=${this.results}
    ></custom-calendar>`;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("tmdb-upcoming", UpcomingComponent);
